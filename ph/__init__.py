"""
Python API for Product Hunt.

@author Arindam Pradhan
@email arindampradhan10@gmail.com
"""

__title__ = 'producthunt'
__author__ = 'Arindam Pradhan'
__license__ = 'MIT'
__copyright__ = 'Copyright 2014 Arindam Pradhan'

from .ph import PH,Product,User